#include "healthPack.h"
#include "common.h"

healthPack::healthPack()
{
	health=1;
	xpos=(float)rand()/RAND_MAX - 0.75f;
	ypos=(float)rand()/RAND_MAX - 0.75f;
	xsize=0.04f;
	ysize=0.04f;
}

void healthPack::draw()
{
	if(collide==false)
	{
		glColor3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
		glVertex3f (xpos, ypos, 0.0); // first corner
		glVertex3f (xpos+xsize, ypos, 0.0); // second corner
		glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
		glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
		glEnd();
	}
}

void healthPack::heal(player *player)
{
	if (collide==false)
	{
		if(player->getHealth()<player->getHealthCap())
		{
			player->getHealth()=player->getHealthCap();
		}

		else
		{
			player->getHealth()=player->getHealth()+health;
			player->getHealthCap()=player->getHealth()+health;
		}
	}
}