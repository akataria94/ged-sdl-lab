#include "game.h"
#include "label.h"
#include "statePlay.h"
#include "stateMainMenu.h"
#include "stateCombat.h"
#include "gameOver.h"
#include "splashScreen.h"

game::game()
{
	currentState = NULL;
}

game::~game()
{

}


void game::setState(gameState * newState) {
	if(newState != NULL) {
		currentState = newState;
		currentState->init(*this);
	}
}


void game::exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

void game::init()
{
	mainMenuState = new stateMainMenu();
	mainMenuState->init(*this);
	combatState = new stateCombat();
	combatState->init(*this);
	playState = new statePlay();
	playState->init(*this);
	gameOverState = new gameOver();
	gameOverState->init(*this);
	splashScreenState = new splashScreen();

	currentState = splashScreenState;
	SDL_GLContext glContext; // OpenGL context handle
    window = setupRC(glContext); // Create window and render context
	if (window !=NULL)
	{
	 currentState->init(*this);
	}
	
}


gameState * game::getPlayState()
{
	return playState;
}

gameState * game::getMainMenuState()
{
	return mainMenuState;
}

gameState * game::getGameOver()
{
	return gameOverState;
}

gameState * game::getCombatState(monster *monster, player *player)
{

	combatState=new stateCombat(monster, player);


	return combatState;
}

gameState * game::getSplashScreen()
{
	return splashScreenState;
}

gameState * game::getNewGame()
{
	playState=new statePlay();

	return playState;
}
// Set up rendering context
// Sets values for, and creates an OpenGL context for use with SDL
SDL_Window * game::setupRC(SDL_GLContext &context)
{
	SDL_Window *window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");

	return window;
}



void game::run(void) 
{
	bool running = true; // set running to true
	SDL_Event sdlEvent;
	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;

			if(currentState != NULL)
				currentState->handleSDLEvent(sdlEvent, *this);
		}

		if(currentState != NULL)
			currentState->draw(window);		
	}

}

