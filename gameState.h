#pragma once

#include <iostream>
#include <sdl\SDL.h>

class game;

class gameState
{
public:

	gameState(){}

	virtual ~gameState() {return;};
	virtual void draw(SDL_Window * window)=0;
	virtual void init(game &context) = 0;
	virtual void handleSDLEvent(SDL_Event const &sdlEvent, game &context) = 0;

};
