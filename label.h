#pragma once

#include <iostream>
#include <sdl/SDL.h>
#include <sdl/SDL_ttf.h>
#include <gl\glew.h>
#include <sstream>
#include <string>


class label
{
public:
	 label();
	 ~label(){glDeleteTextures(1, &texture);}
	 void textToTexture( const char * str);
	 void draw(float x, float y);


private:
	GLuint texture;
	GLuint height;
	GLuint width;
	TTF_Font* textFont;	

};