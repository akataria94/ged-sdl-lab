#include "stimulant.h"
#include "common.h"

stimulant::stimulant()
{
	speed=1;
	xpos=(float)rand()/RAND_MAX - 0.75f;
	ypos=(float)rand()/RAND_MAX - 0.75f;
	xsize=0.04f;
	ysize=0.04f;
}

void stimulant::draw()
{
	if(collide==false)
	{
		glColor3f(0.0,1.0,0.0);
		glBegin(GL_POLYGON);
		glVertex3f (xpos, ypos, 0.0); // first corner
		glVertex3f (xpos+xsize, ypos, 0.0); // second corner
		glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
		glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
		glEnd();
	}
}

void stimulant::speedUp(player* player)
{
	if (collide==false)
	{
		player->getSpeed()=player->getSpeed()+speed;
	}
}