///////////////////////////////////////////////////////////////////////////////////
//---------------------------------GameOver----------------------------------------/
//   Game over screen. On pressing spacebar you are returned to the main menu.    //
//-----------------------------------Functions-----------------------------------//
//                                                                           	//
// init(game &context)	- Initialises game screen                               //
// draw(SDL_Window * window)- draws screen and displays labels                   //
//                                                                               //
// handleSDLEvent(SDL_Event const &sdlEvent, game &context)-event handler for the//
//                                                          state.               //

#pragma once

#include "gameState.h"
#include <iostream>
#include <ctime>
#include "label.h"
#include "Game.h"

class gameOver : public gameState
{
public:
	gameOver();
	~gameOver();

	void Init(game * context);
	void init(game &context);
	void draw(SDL_Window * window);
    void handleSDLEvent(SDL_Event const &sdlEvent, game &context);

private:

	label *gameOverMessage;

	SDL_Window *window;
	TTF_Font* textFont;	
};