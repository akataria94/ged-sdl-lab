///////////////////////////////////////////////////////////////////////////////////
//----------------------------------StatePlay------------------------------------//
//      This state will display the credits for 3 seconds before switching to    //
//    main menu state.                                                         	 //
//-----------------------------------Functions-----------------------------------//
//                                                                           	 //
// init(game &context)	- Initialises game screen                                //
// draw(SDL_Window * window)- draws the map screen and randomly places monsters  //
//   across the screen                                                           //
// handleSDLEvent(SDL_Event const &sdlEvent, game &context)-event handler for the//
//   state. Checks each and every collision and changes state accordingly.       //
// isAlive() - check to see if the passed in monster is alive.                   //

#pragma once

#include "gameState.h"
#include <ctime>
#include "label.h"
#include "player.h"
#include "monster.h"
#include "gameState.h"
#include "Game.h"
#include "raider.h"
#include "brute.h"
#include "fodder.h"
#include "items.h"

class statePlay : public gameState
{
public:
	
	statePlay();
	~statePlay();

	void Init(game * context);
	void init(game &context);
	void draw(SDL_Window * window);
    void handleSDLEvent(SDL_Event const &sdlEvent, game &context);
	void start();// Intialises member variables
	bool isAlive(monster *monster);

private:

	label *playerLabel;
	label *scores;
	player *player1;
	raider *raider1;
	brute *brute1;
	brute *brute2;
	brute *brute3;
	fodder *fod[5];
	healthPack *healthPack1;
	combatPack *combatPack1;
	stimulant *stimulant1;

	int score;

	SDL_Window *window;
	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime; // use this to track time between frames
	
	TTF_Font* textFont;	

};