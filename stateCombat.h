///////////////////////////////////////////////////////////////////////////////////
//---------------------------------StateCombat-----------------------------------/
//      This state sets the combat mode for the game by taking a pointer to     //
//    monster and player. It then redraws each one and implments the battle      //
//    mechanics.                                                                //
//-----------------------------------Functions-----------------------------------//
//                                                                           	//
// init(game &context)	- Initialises game screen                               //
// draw(SDL_Window * window)- draws the monster and player                       //
// getItem()- sets the possibility of getting a healthpack(60%),combatPack(20%) //
//  and stimulant(20%).                                                          // 
// battle()-the battle mechanics for a single turn of the game.                 //
// handleSDLEvent(SDL_Event const &sdlEvent, game &context)-event handler for the//
//    game. Everytime spacebar is pressed it implements the battle mechanics it  //
//    also contains logic for player death and monster death.                    //
//checkPlayerDeath()-Returns a boolean depending on whether the player is dead or not//
//checkMonsterDeath()-Returns a boolean depending on whether the monster is dead or not//

#pragma once

#include "gameState.h"
#include "game.h"
#include "monster.h"
#include "label.h"
#include "player.h"
#include "items.h"

class stateCombat : public gameState
{
public:
	stateCombat();
	stateCombat(monster * monster, player * player);
	~stateCombat(){};

	void Init(game * context);
	void init(game &context);
	void draw(SDL_Window * window);
	void getItem();
	void battle();
	void handleSDLEvent(SDL_Event const &sdlEvent, game &context);
	bool checkPlayerDeath();
	bool checkMonsterDeath();

private:
	monster *newMonster;
	player *player1;
	label *playerHealthLabel;
	label *monsterHealthLabel;
	healthPack* hpack;
	combatPack* cpack;
	stimulant* stim;
	int playerHealth;
	int monsterHealth;
	int playerStrength;
	int restoreHealth; // Required for the continue game feature.
	int monsterStrength;
	bool firstHit;

	SDL_Window *window;
};