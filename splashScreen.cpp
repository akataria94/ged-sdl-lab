#include "splashScreen.h"
#include <ctime>

#include <windows.h>

splashScreen::splashScreen()
{
	credit = new label();
}

splashScreen::~splashScreen()
{
	delete credit;
}

void splashScreen::init(game &context)
{
	glClearColor(0.556, 0.137, 0.18, 0.0);

}

void splashScreen::draw(SDL_Window * window)
{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		credit->textToTexture("Aman Kataria B00239831");
		credit->draw(-0.4,0.0);

		SDL_GL_SwapWindow(window);
		Sleep(3000);
		context->setState(context->getMainMenuState());
}

void splashScreen::handleSDLEvent(SDL_Event const &sdlEvent, game &context)
{
	this->context = &context;
}