
#include <iostream>
#include <sdl/SDL.h>
#include <sdl/SDL_ttf.h>
#include <gl\glew.h>

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>
#include "game.h"

#include "gameState.h"
#include "statePlay.h"
#include "stateMainMenu.h"
#include "common.h"

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif


int main(int argc, char *argv[])
{	
    game * myGame = new game();

	myGame->init();
	myGame->run();

	delete myGame;
    return 0;

}