#include "fodder.h"

fodder::fodder()
{
	xpos=(float)rand()/RAND_MAX - 0.45f;
	ypos=(float)rand()/RAND_MAX - 0.75f;
	xsize=0.06f;
	ysize=0.06f;

	fodderLabel=new label();

	health=3;
	strength=3;
	speed=7;
}

void fodder::draw()
{
	glColor3f(0.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex3f (xpos, ypos, 0.0); // first corner
	glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
}

void fodder::setLabel()
{
	fodderLabel->textToTexture("Fodder");
	fodderLabel->draw(xpos+xsize/2.0f,ypos+ysize);
}

bool fodder::update(player * player)
{
	if ( (getXpos() >= player->getXpos()) && (getXpos()+getXsize() <= player->getXpos()+player->getXsize())	
		&& ((getYpos() >= player->getYpos()) && (getYpos()+getYsize() <= player->getYpos()+player->getYsize() ))) // checks to see if player surrounds fodder
	{
		return true;
	}

	else
	{return false;}
}
