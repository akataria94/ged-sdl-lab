#include "stateCombat.h"
#include "player.h"
#include "items.h"

stateCombat::stateCombat()
{

}

stateCombat::stateCombat(monster *monster, player *player)
{
	newMonster=monster;
	player1=player;
	playerHealthLabel=new label();
	monsterHealthLabel=new label();
	playerHealth=player1->getHealth();
	monsterHealth=newMonster->getHealth();
	restoreHealth=monsterHealth;
	playerStrength=player1->getStrength();
	monsterStrength=newMonster->getStrength();
	hpack=new healthPack();
	cpack=new combatPack();
	stim=new stimulant();

	//Check to see who gets the first turn
	if (player1->getSpeed()>newMonster->getSpeed())
	{
		firstHit=true;
	}

	else
	{
		firstHit=false;
	}
}

void stateCombat::init(game &context)
{
	glClearColor(0.556, 0.137, 0.18, 0.0);
}

void stateCombat::draw(SDL_Window *window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	newMonster->setPosition(0.75f, 0.0f);
	player1->setPosition(-0.75f, 0.0f);
	newMonster->draw();
	player1->draw();

	//Display health of player and monster
	std::stringstream strStream;
	std::stringstream strStream2;
	strStream << "Health:" << playerHealth;
	strStream2 << "Health:" << monsterHealth;
	playerHealthLabel->textToTexture(strStream.str().c_str());
	playerHealthLabel->draw(-0.8f,0.6f);
	monsterHealthLabel->textToTexture(strStream2.str().c_str());
	monsterHealthLabel->draw(0.5f,0.6f);

	SDL_GL_SwapWindow(window);
}

void stateCombat::getItem()
{
	//Creates the required chances of getting an item after a battle.
	
	int number=rand() % 10+1;
	if (number<=6)
	{
		hpack->heal(player1);
		number=11; //is needed to reset the chances, next battle
	}

	int number2=rand() % 10+1;
	if (number2<=2)
	{
		cpack->strengthUp(player1);
		number=11;
	}

	int number3=rand() % 10+1;
	if (number<=2)
	{
		stim->speedUp(player1);
		number=11;
	}
}

void stateCombat::battle()
{

	if(firstHit==true)
	{
		monsterHealth=monsterHealth-playerStrength; 
		newMonster->getHealth()=monsterHealth;
		firstHit=false;
	}

	else  
	{
		playerHealth=playerHealth-monsterStrength;
		player1->getHealth()=playerHealth;
		firstHit=true;
	}

}

bool stateCombat::checkPlayerDeath()
{
	if (player1->getHealth()<=0)
	{
		return true;	
	}


	else
		return false;
}

bool stateCombat::checkMonsterDeath()
{
	if (newMonster->getHealth()<=0)
	{
		return true;	
	}

	else
		return false;
}



void stateCombat::handleSDLEvent(SDL_Event const &sdlEvent, game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
				switch( sdlEvent.key.keysym.sym )
				{
				
					case SDLK_SPACE:
						battle();

						if(checkMonsterDeath()==true)
						{
								context.setState(context.getPlayState());
								player1->getHealth()=playerHealth+((player1->getHealthCap()-playerHealth)/2); // restores half the players health after combat

								if (player1->getHealth()>player1->getHealthCap()) //makes sure the player doesn't gain extra health
								{
									player1->getHealth()=player1->getHealthCap();
									getItem();
								}
						}

						if(checkPlayerDeath()==true)
						{
							context.setState(context.getGameOver());
							newMonster->getHealth()=restoreHealth;
							player1->getHealth()=player1->getHealthCap();
						}
				}
	}

}