///////////////////////////////////////////////////////////////////////////////////
//---------------------------------combatPack-----------------------------------/
// An item used to increase player strenght by 2.                                //
//-----------------------------------Functions-----------------------------------//
//                                                                           	//
// draw() - draws the object on screen, a much smaller size than a monster and   //
//    no label.                                                                  //
// strengthUp() - Increases strength of passed in player by 2.                   //

#include "item.h"
#include "player.h"

class combatPack : public item
{
private:
	int strength;

public:
	combatPack();
	~combatPack(){};
	void draw();
	void strengthUp(player *player);
};
