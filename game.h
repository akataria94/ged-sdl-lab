#pragma once

#include <iostream>
#include <sdl/SDL.h>
#include <sdl/SDL_ttf.h>
#include <gl\glew.h>

#include "gameState.h"

#include "label.h"
#include "monster.h"
#include "player.h"
// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>
// iostream for cin and cout
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>

class game
{
public:

	 game();
	 ~game();

	 SDL_Window * setupRC(SDL_GLContext &context);
	 void run(void);
	 void init(void);
	
	 void setState(gameState * newState);
	 gameState *getState();
	 gameState *getPlayState();
	 gameState *getMainMenuState();
	 gameState *getCombatState(monster *monster, player *player);
	 gameState *getGameOver();
	 gameState *getNewGame();
	 gameState *getSplashScreen();

	 void exitFatalError(char *message);

private:

	SDL_Window *window;

	label * splash;

	gameState * playState;
	gameState * mainMenuState;
	gameState * currentState;
	gameState * combatState;
	gameState * gameOverState;
	gameState * splashScreenState;
	game *context;
};