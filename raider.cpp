#include "raider.h"
#include "player.h"
#include "common.h"

raider::raider()
{
	xpos=(float)rand()/RAND_MAX - 0.65f;
	ypos=(float)rand()/RAND_MAX - 0.75f;
	xsize=0.18f;
	ysize=0.18f;

	raiderLabel=new label();

	health=12;
	strength=7;
	speed=12;
}

void raider::draw()
{
	glColor3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
	glVertex3f (xpos, ypos, 0.0); // first corner
	glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
}

void raider::setLabel()
{
	raiderLabel->textToTexture("Raider");
	raiderLabel->draw(xpos+xsize/2.0f,ypos+ysize);
}

bool raider::update(player * player)
{
	if ( (getXpos() <= player->getXpos()) && (getXpos()+getXsize() >= player->getXpos()+player->getXsize())	// checks to see if raide surrounds player
		&& ((getYpos() <= player->getYpos()) && (getYpos()+getYsize() >= player->getYpos()+player->getYsize() )))
	{
		return true;
	}

	else
	{return false;}
}


