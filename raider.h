///////////////////////////////////////////////////////////////////////////////////
//-------------------------------------Raider--------------------------------------/
//  This is the class that contains all the features of the raider monster.       //
//-----------------------------------Functions-----------------------------------//
//                                                                               //
// draw()- draws raider on screen at a random position.                           //
// setLabel() - sets the label for the raider.                                    //
// update()-Returns a true or false value depending on whether the raider and     //
//    player have collided.                                                      //

#pragma once

#include "monster.h"
#include "label.h"
#include "player.h"

class raider:public monster
{
public:
	raider();
	~raider(){};
	void draw();
	void setLabel();
	bool update(player * player);

	float getXpos(){return xpos;}
	float getYpos(){return ypos;}
	float getXsize(){return xsize;}
	float getYsize(){return ysize;}


private:
	label *raiderLabel;
};