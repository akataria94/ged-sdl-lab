///////////////////////////////////////////////////////////////////////////////////
//------------------------------------Monster-------------------------------------/
//      Base class for all of the monsters in the game. Contains a few methods  //
//    required by all of its subclasses                                       	//


#pragma once
#include "player.h"

class monster
{
protected:
	int health;
	int strength;
	int speed;

public:
	float xpos;
	float ypos;
	float xsize;
	float ysize;

	int& getHealth(){return health;}
	int& getStrength(){return strength;}
	int& getSpeed(){return speed;}

	monster(){};

	virtual void draw()=0;
	virtual void setLabel()=0;
	void setPosition(float X, float Y)
	{
		xpos=X;
		ypos=Y;
	}
	bool isAlive()
	{
		if (getHealth()>0)
			return true;
		else
			return false;
	}

};