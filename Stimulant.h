///////////////////////////////////////////////////////////////////////////////////
//---------------------------------stimulant-------------------------------------/
// An item used to increase player speed by 1.                                   //
//-----------------------------------Functions-----------------------------------//
//                                                                             	//
// draw() - draws the object on screen, a much smaller size than a monster and   //
//    no label.                                                                  //
// speedUp() - Increases speed of passed in player by 1.                        //

#include "item.h"
#include "player.h"

class stimulant : public item
{
private:
	int speed;

public:
	stimulant();
	~stimulant(){};
	void draw();
	void speedUp(player *player);
};