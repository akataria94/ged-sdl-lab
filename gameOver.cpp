#include "gameOver.h"
#include "label.h"

gameOver::gameOver()
{
	gameOverMessage = new label();
}

gameOver::~gameOver()
{
	delete gameOverMessage;
}

void gameOver::init(game &context)
{
	glClearColor(0, 0, 0, 0.0);
}

void gameOver::draw(SDL_Window *window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gameOverMessage->textToTexture("Game is Over!");
	gameOverMessage->draw(-0.4f,0.0f);

	SDL_GL_SwapWindow(window);
	
}

void gameOver::handleSDLEvent(SDL_Event const &sdlEvent, game &context)
{
		if (sdlEvent.type==SDL_KEYDOWN)
	{
		switch (sdlEvent.key.keysym.sym)
		{
		case SDLK_SPACE:case SDLK_RETURN:
			context.setState(context.getMainMenuState());
		}
	}

}