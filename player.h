#pragma once

class player
{
private:
	int health;
	int strength;
	int speed;
	int healthcap;

public:
	float xpos;
	float ypos;
	float xsize;
	float ysize;

public:
	float getXpos(){return xpos;}
	float getYpos(){return ypos;}
	float getXsize(){return xsize;}
	float getYsize(){return ysize;}

	int& getHealthCap(){return healthcap;}
	int& getHealth(){return health;}
	int& getStrength(){return strength;}
	int& getSpeed(){return speed;}

	player(float posx, float posy ,float sizex, float sizey);
	void draw();
	void setPosition(float X, float Y);
	~player();
};