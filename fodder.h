///////////////////////////////////////////////////////////////////////////////////
//-------------------------------------Fodder--------------------------------------/
//  This is the class that contains all the features of the fodder monster.       //
//-----------------------------------Functions-----------------------------------//
//                                                                               //
// draw()- draws fodder on screen at a random position.                           //
// setLabel() - sets the label for the fodder.                                    //
// update()-Returns a true or false value depending on whether the fodder and     //
//    player have collided.                                                      //

#pragma once

#include "monster.h"
#include "label.h"
#include "player.h"

class fodder : public monster
{
	private:
	label *fodderLabel;

public:
	fodder();
	~fodder(){};
	void draw();
	void setLabel();
	bool update(player *player);
	float getXpos(){return xpos;}
	float getYpos(){return ypos;}
	float getXsize(){return xsize;}
	float getYsize(){return ysize;}
};