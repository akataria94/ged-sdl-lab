#include "statePlay.h"
#include "game.h"
#include "label.h"
#include "common.h"
#include "stateCombat.h"


statePlay::statePlay()
{
	scores=new label();
	playerLabel=new label();
	player1=new player(0.0f, 0.0f, 0.12f, 0.12f);
	raider1=new raider();
	brute1=new brute();
	brute2=new brute();
	brute3=new brute();
	for(int i=0;i<5;i++)
	{
		fod[i]=new fodder();
	}
	healthPack1=new healthPack();
	combatPack1=new combatPack();
	stimulant1=new stimulant();
	start();
}


void statePlay::init(game &context)
{
	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour

   std::srand((unsigned)std::time(NULL) );
   lastTime = clock();
}

statePlay::~statePlay() {
	delete scores;
	delete player1;
	delete playerLabel;
	delete raider1;
	delete brute1;
	delete brute2;
	delete brute3;
	for(int i=0;i<5;i++)
		{
			delete fod[i];
		}
	delete combatPack1;
	delete healthPack1;
	delete stimulant1;
}

void statePlay::start()
{
	 score = 0;
}


bool statePlay::isAlive(monster *monster)
{
	if(monster->getHealth()>0)
		return true;
	else
		return false;
}

void statePlay::draw(SDL_Window *window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	player1->draw();
	playerLabel->textToTexture("Player");
	playerLabel->draw(player1->xpos+(player1->xsize/2.0f), player1->ypos+player1->ysize);

	if(raider1!=NULL) {
		raider1->draw();
		raider1->setLabel();
	}

	if(brute1->isAlive()==true)
	{
		brute1->draw();
		brute1->setLabel();
	}

	if(brute2->isAlive()==true)
	{
		brute2->draw();
		brute2->setLabel();
	}

	if(brute3->isAlive()==true)
	{
		brute3->draw();
		brute3->setLabel();
	}

	for(int i=0;i<5;i++)
	{
		if(fod[i]->isAlive()==true)
		{
			fod[i]->draw();
			fod[i]->setLabel();
		}
	}

	healthPack1->draw();
	combatPack1->draw();
	stimulant1->draw();


    // Calculate ms/frame
    // Some OpenGL drivers will limit the frames to 60fps (16.66 ms/frame)
    // If so, expect to see the time to rapidly switch between 16 and 17...
    glColor3f(1.0,1.0,1.0);
    currentTime = clock();
    // On some systems, CLOCKS_PER_SECOND is 1000, which makes the arithmetic below redundant
    // - but this is not necessarily the case on all systems
    float milliSecondsPerFrame = ((currentTime - lastTime)/(float)CLOCKS_PER_SEC*1000);

    // Print out the score and frame time information
    std::stringstream strStream;
    strStream << "Money:� " << score;
	strStream << "         Health:" << player1->getHealth();
    strStream << "            ms/frame: " << milliSecondsPerFrame;
	scores->textToTexture(strStream.str().c_str());
	scores->draw(-0.9f,0.9f);
    lastTime = clock();

	SDL_GL_SwapWindow(window);
}

void statePlay::handleSDLEvent(SDL_Event const &sdlEvent, game &context)

{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
			case SDLK_UP:
			case 'w': case 'W': 
				player1->ypos += 0.05f;
				break;
			case SDLK_DOWN:
			case 's': case 'S':
				player1->ypos -= 0.05f;
				break;
			case SDLK_LEFT:
			case 'a': case 'A': 
				player1->xpos -= 0.05f;
				break;
			case SDLK_RIGHT:
			case 'd': case 'D':
				player1->xpos += 0.05f;
				break;
			case SDLK_ESCAPE:
				context.setState(context.getMainMenuState());
			default:
				break;
		}
	}
	//COLLISIONS:

	if(raider1 != NULL) {
		if (raider1->update(player1)==true)
		{
			context.setState(context.getCombatState(raider1, player1));
			score=score+100; //Increase the money
			raider1 = NULL;
		}
	}

		if (brute1->update(player1)==true)
	{
		context.setState(context.getCombatState(brute1, player1));
		score=score+10;
	}

		if (brute2->update(player1)==true)
	{
		context.setState(context.getCombatState(brute2, player1));
		score=score+10;
	}

		if (brute3->update(player1)==true)
	{
		context.setState(context.getCombatState(brute3, player1));
		score=score+10;
	}

		for(int i=0; i<5; i++)
		{
			if(fod[i]->update(player1)==true)
			{
				context.setState(context.getCombatState(fod[i], player1));
				score=score+1;
			}
		}

		if (healthPack1->update(player1)==true)
		{
			healthPack1->heal(player1);
			healthPack1->collide=true;
		}

		if (combatPack1->update(player1)==true)
		{
			combatPack1->strengthUp(player1);
			combatPack1->collide=true;
		}

		if (stimulant1->update(player1)==true)
		{
			stimulant1->speedUp(player1);
			stimulant1->collide=true;
		}
}

