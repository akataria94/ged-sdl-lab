///////////////////////////////////////////////////////////////////////////////////
//-------------------------------------Brute---------------------------------------/
//  This is the class that contains all the features of the brute monster.        //
//-----------------------------------Functions-----------------------------------//
//                                                                               //
// draw()- draws brute on screen at a random position.                           //
// setLabel() - sets the label for the brute.
// update()-Returns a true or false value depending on whether the brute and     //
//    player have collided.                                                      //

#pragma once

#include "monster.h"
#include "label.h"
#include "player.h"

class brute:public monster
{
private:
	label *bruteLabel;

public:
	brute();
	~brute(){delete bruteLabel;}
	void draw();
	void setLabel();
	bool update(player *player);
	float getXpos(){return xpos;}
	float getYpos(){return ypos;}
	float getXsize(){return xsize;}
	float getYsize(){return ysize;}
};