#include "player.h"
#include "common.h"

player::player(float posx, float posy ,float sizex, float sizey)
{
	
	xpos=posx;
	ypos=posy;
	xsize=sizex;
	ysize=sizey;

	healthcap=10;
	health=10;
	strength=10;
	speed=10;
}

player::~player()
{

}

void player::draw()
{
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	glVertex3f (xpos, ypos, 0.0); // first corner
	glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
}

void player::setPosition(float X, float Y)
{
	xpos=X;
	ypos=Y;
}