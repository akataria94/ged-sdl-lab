///////////////////////////////////////////////////////////////////////////////////
//---------------------------------StateMainMenu-----------------------------------/
//      This state will display the main menu, as of now the new game, continue   //
//    game, credits and quit work. If you contiue game without having an existing //
//    game, it will simply create a new game.                                     //
//-----------------------------------Functions-----------------------------------//
//                                                                           	//
// init(game &context)	- Initialises game screen                               //
// draw(SDL_Window * window)- draws screen and displays labels                   //
//                                                                               //
// handleSDLEvent(SDL_Event const &sdlEvent, game &context)-event handler for the//
//                                                          state.               //

#pragma once

#include "gameState.h"
#include <iostream>
#include <ctime>
#include "label.h"
#include "Game.h"

class stateMainMenu : public gameState
{
public:
	stateMainMenu();
	~stateMainMenu();

	void Init(game * context);
	void init(game &context);
	void draw(SDL_Window * window);
    void handleSDLEvent(SDL_Event const &sdlEvent, game &context);

private:

	label *newGame;
	label *continueGame;
	label *loadGame;
	label *quit;
	label *credits;

	SDL_Window *window;
	TTF_Font* textFont;	
};
