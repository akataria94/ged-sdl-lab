///////////////////////////////////////////////////////////////////////////////////
//---------------------------------Label Class---------------------------------//
//      This is a class used to generate labels for all object in the game as  //                                                               	
//    well as display any text.                                                //
//                                                                           	//
//-----------------------------------Functions----------------------------------//
//                                                                           	//
// textToTexture - sets the text that is inputed in the argument to be displayed//
// draw - Draws texure at co-ordinates inputed in argument  //


#include "label.h"
#include "game.h"
#include <Windows.h>

label::label()
{ 
	glGenTextures(1, &texture);
	textFont = NULL;
}

void label::textToTexture(const char * str)
{
	if(!textFont) {
		textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	}
	
	SDL_Surface *stringImage;
	SDL_Color colour = { 255, 255, 0 };
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	//if (stringImage == NULL)
		//exitFatalError("String surface not created.");

	GLuint colours = stringImage->format->BytesPerPixel;
	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;
	
	// Copy the SDL surface to an OpenGL texture

	glBindTexture(GL_TEXTURE_2D, texture); 
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, stringImage->w, stringImage->h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
	// SDL surface no longer required
	SDL_FreeSurface(stringImage);

	height=stringImage->h;
	width=stringImage->w;
}

void label::draw(float x, float y)
{
	// Draw texture here
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex3f (x,y, 0.0); // first corner
	  glTexCoord2d(1,1);
      glVertex3f (x+0.002f*width, y, 0.0); // second corner
	  glTexCoord2d(1,0);
      glVertex3f (x+0.002f*width, y+0.002f*height, 0.0); // third corner
	  glTexCoord2d(0,0);
      glVertex3f (x, y+0.002f*height, 0.0); // fourth corner
    glEnd();

	// As this texture is not being kept, delete it here
	// For labels that do not change, it would be better to not have to recreate them every
	// frame
	glDisable(GL_TEXTURE_2D);
}