///////////////////////////////////////////////////////////////////////////////////
//----------------------------------healthPack------------------------------------/
// An item used to restore player health or increase by 1 if full.               //
//-----------------------------------Functions-----------------------------------//
//                                                                           	 //
// draw() - draws the object on screen, a much smaller size than a monster and   //
//    no label.                                                                  //
// heal() - heals the player if not healed otherwise increases health by 1.      //

#include "item.h"
#include "player.h"

class healthPack : public item
{
private:
	int health;

public:
	healthPack();
	~healthPack(){};
	void draw();
	void heal(player *player);

};