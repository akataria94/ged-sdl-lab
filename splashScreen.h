///////////////////////////////////////////////////////////////////////////////////
//---------------------------------SplashScreenState-------------------------------/
//      This state will display the credits for 3 seconds before switching to   //
//    main menu state.                                                         	//
//-----------------------------------Functions-----------------------------------//
//                                                                           	//
// init(game &context)	- Initialises game screen                               //
// draw(SDL_Window * window)- draws screen and displays label and changes state  //
//                             after 3 secs.                                    //
// handleSDLEvent(SDL_Event const &sdlEvent, game &context)-event handler for the//
//                                                          state.               //
#pragma once

#include "gameState.h"
#include "game.h"
#include "label.h"
#include <ctime>

class splashScreen : public gameState
{
private:
	label *credit;
	game * context;

public:
	splashScreen();
	~splashScreen();
	void init(game &context);
	void draw(SDL_Window * window);
	void handleSDLEvent(SDL_Event const &sdlEvent, game &context);
};