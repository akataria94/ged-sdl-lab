#pragma once
#include "player.h"

class item
{
public:
	item(){collide=false;}
	virtual void draw()=0;
	bool update(player * player);
	bool collide;

	float getXpos(){return xpos;}
	float getYpos(){return ypos;}
	float getXsize(){return xsize;}
	float getYsize(){return ysize;}

	float xpos;
	float ypos;
	float xsize;
	float ysize;

};