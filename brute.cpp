#include "brute.h"
#include "label.h"

brute::brute()
{
	xpos=(float)rand()/RAND_MAX - 0.55f;
	ypos=(float)rand()/RAND_MAX - 0.55f;
	xsize=0.07f;
	ysize=0.07f;

	bruteLabel=new label();

	health=12;
	strength=10;
	speed=6;
}

void brute::draw()
{
	glColor3f(1.0,1.0,0.0);
	glBegin(GL_POLYGON);
	glVertex3f (xpos, ypos, 0.0); // first corner
	glVertex3f (xpos+xsize, ypos, 0.0); // second corner
	glVertex3f (xpos+xsize, ypos+ysize, 0.0); // third corner
	glVertex3f (xpos, ypos+ysize, 0.0); // fourth corner
	glEnd();
}

void brute::setLabel()
{
	bruteLabel->textToTexture("Brute");
	bruteLabel->draw(xpos+xsize/2.0f,ypos+ysize);
}

bool brute::update(player * player)
{
	if ( (getXpos() >= player->getXpos()) && (getXpos()+getXsize() <= player->getXpos()+player->getXsize())	
		&& ((getYpos() >= player->getYpos()) && (getYpos()+getYsize() <= player->getYpos()+player->getYsize() ))) //Checks to see if player surrounds brute.
	{
		return true;
	}

	else
	{return false;}
}