#include "stateMainMenu.h"
#include "game.h"
#include "label.h"
#include "common.h"

stateMainMenu::stateMainMenu()
{
	newGame= new label();
	continueGame= new label();
	loadGame= new label();
	quit= new label();
	credits=new label();
}

void stateMainMenu::init(game &context)
{
	glClearColor(0.556, 0.137, 0.18, 0.0);
}


stateMainMenu::~stateMainMenu()
{
	delete newGame;
	delete continueGame;
	delete loadGame;
	delete quit;
	delete credits;
}

void stateMainMenu::draw(SDL_Window *window)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	newGame->textToTexture("New Game(N)");
	newGame->draw(-0.5f,0.4f);
	
	continueGame->textToTexture("Continue Game(C)");
	continueGame->draw(-0.5f,0.3f);

	loadGame->textToTexture("Load Game(L)");
	loadGame->draw(-0.5f,0.2f);

	quit->textToTexture("Quit(Q)");
	quit->draw(-0.5f, 0.1f);

	credits->textToTexture("Credits(R)");
	credits->draw(-0.5f, 0.0f);


	SDL_GL_SwapWindow(window);
	
}

void stateMainMenu::handleSDLEvent(SDL_Event const &sdlEvent, game &context)
{
	if (sdlEvent.type==SDL_KEYDOWN)
	{
		switch (sdlEvent.key.keysym.sym)
		{
		case 'n' :case 'N':
			context.setState(context.getNewGame());
			break;
		case 'c' :case 'C':
			context.setState(context.getPlayState());
			break;
		case SDLK_ESCAPE:
			context.exitFatalError("");
			break;
		case 'q' :case 'Q':
			context.exitFatalError("");
			break;
		case 'r' :case 'R':
			context.setState(context.getSplashScreen());

		}
	}
}

